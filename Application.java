//Polina Yankovich 1834306
public class Application{
	public static void main(String[] args){
		//cat1
		Cat cat1=new Cat();
		cat1.name="niki";
		cat1.colour="black";
		cat1.age=11;

		//cat2
		Cat cat2=new Cat();
		cat2.name="coco";
		cat2.colour="blonde";
		cat2.age=1;
		
		cat1.OldOrYoung();
		cat1.NameAndColour();
		
		cat2.OldOrYoung();
		cat2.NameAndColour();
		
		
		Cat[] cats=new Cat[3];
		cats[0]=cat1;
		cats[1]=cat2;
		
		cats[2]=new Cat();
		
		System.out.println("the name of the first cat is "+cats[2].name);
		
}}